# -*- coding: utf-8 -*-
"""
Created on Fri May 14 11:49:57 2021

@author: Joana Santos
"""
def data_especifica():
    from datetime import date
    while True:
          try:
              data_ano = int(input ("Qual o ano que pretende ver? (2020/2021) "))
              break
          except ValueError:
              print ("Ano inválido.")
    while data_ano not in {2020,2021}:
         print ("Ano inválido.")
         while True:
             try:
                 data_ano = int(input ("Qual o ano que pretende ver? (2020/2021) "))
                 break
             except ValueError:
                 print ("Ano inválido.")
    while True:
         try:
             data_mes = int(input ("Qual o mês que pretende ver? (Insira um número de 1 a 12) ")) 
             break
         except ValueError:
             print ("Mês inválido.")
    while data_mes not in {1,2,3,4,5,6,7,8,9,10,11,12}:
         print("Mês inválido")
         while True:
             try:
                 data_mes = int(input ("Qual o mês que pretende ver? (Insira um número de 1 a 12) ")) 
                 break
             except ValueError:
                 print ("Mês inválido.")
    if len(str(data_mes)) == 1:
         data_mes = "0"+ str(data_mes)
    while True:
         try:
             data_dia = int(input ("Qual o dia que pretende ver? (Escreva um número inteiro) ")) 
             break
         except ValueError:
             print ("Dia inválido.")
    if data_mes == "02" and data_ano == 2020:
         while data_dia > 29 or data_dia < 1:
             print("Dia inválido")
             while True:
                 try:
                     data_dia = int(input ("Qual o dia que pretende ver? (Escreva um número inteiro) ")) 
                     break
                 except ValueError:
                     print ("Dia inválido.")
    elif data_mes == "02" and data_ano == 2021:
         while data_dia < 1 or data_dia > 28:
             print("Dia inválido")
             while True:
                 try:
                     data_dia = int(input ("Qual o dia que pretende ver? (Escreva um número inteiro) ")) 
                     break
                 except ValueError:
                     print ("Dia inválido.")
    elif int(data_mes) in {1,3,5,7,8,10,12}:
         while data_dia < 1 or data_dia > 31:
             print("Dia inválido")
             while True:
                 try:
                     data_dia = int(input ("Qual o dia que pretende ver? (Escreva um número inteiro) ")) 
                     break
                 except ValueError:
                     print ("Dia inválido.")
    elif int(data_mes) in {4,6,9,11}:
         while data_dia < 1 or data_dia > 30:
             print("Dia inválido")
             while True:
                 try:
                     data_dia = int(input ("Qual o dia que pretende ver? (Escreva um número inteiro) ")) 
                     break
                 except ValueError:
                     print ("Dia inválido.")
    print (" ")
    data = date(int(data_ano), int(data_mes), int(data_dia)) 
    return (data)