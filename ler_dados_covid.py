#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  5 14:46:59 2021

@author: sarapereira
"""

import urllib

link = 'https://covid.ourworldindata.org/data/owid-covid-data.csv'
nomef = 'owid-covid-data.csv'
urllib.request.urlretrieve(link, nomef)

import csv

with open(nomef, 'r', newline='') as f:
    leitor = csv.DictReader (f, delimiter=',')
    dados = list(leitor)

