# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 21:38:45 2021

@author: Utilizador
"""

from ler_dados_covid import dados

from data_especifica import data_especifica

### Escolher dados de 1/mais continentes###
linha_de_iguais = 60*"="
dados_c = "Dados do continente"
umc = "1: Obter dados de um continente"
doisc = "2: Obter dados de mais que um continente"
print(f"{linha_de_iguais:^60s}")
print(f"{dados_c:^60s}")
print(f"{linha_de_iguais:^60s}")
print(f"{umc:^60s}")
print(f"{doisc:^60s}")
while True:
        try:
            escolha = int(input ("Quer obter dados de 1 ou mais continentes? "))
            break
        except ValueError:
            print ("Escolha inválida.")
while escolha not in {1,2}:
    print ("Escolha inválida.")
    while True:
        try:
            escolha = int(input ("Quer obter dados de 1 ou mais continentes? "))
            break
        except ValueError:
            print ("Escolha inválida.")
print(" ")

titulo = "Lista dos continentes:"
print(f"{linha_de_iguais:^60s}")
print(f"{titulo:^60s}")
print(f"{linha_de_iguais:^60s}")
africa = "1: África"
a_n = "2: América do Norte"
a_s = "3: América do Sul"
asia = "4: Ásia"
europa = "5: Europa"
oceania = "6: Oceania"
print(f"{africa:^60s}")
print(f"{a_n:^60s}")
print(f"{a_s:^60s}")
print(f"{asia:^60s}")
print(f"{europa:^60s}")
print(f"{oceania:^60s}")
print(" ")

###Escolher 1 continente###
if escolha == 1:
    while True:
        try:
            continente= int(input("Deseja saber os dados de que continente? "))
            break
        except ValueError:
            print ("Escolha inválida.")
    while continente not in {1, 2, 3, 4, 5, 6}:
        print ("Escolha inválida.")
        while True:
            try:
                continente= int(input("Deseja saber os dados de que continente? "))
                break
            except ValueError:
                print ("Escolha inválida.")
    if continente == 1:
        n = "Africa"
        c0 = "África"
    elif continente == 2:
        n = "North America"
        c0 = "América do Norte"
    elif continente == 3:
        n = "South America"
        c0 = "América do Sul"
    elif continente == 4:
        n = "Asia"
        c0 = "Ásia"
    elif continente == 5:
        n = "Europe"  
        c0 = "Europa"
    else:
        n = "Oceania"
        c0 = "Oceania"
    i1 = 0
    while dados[i1]['location'] != n :
        i1 = i1+1
    print (" ")      
    print (c0 + " tem uma população igual a " + dados[i1]["population"])
    print (" ")        

###Escolher mais continentes###
elif escolha == 2:
    while True:
        try:
            nc = int(input("Quer obter dados de quantos continentes? "))
            break
        except ValueError:
            print ("Escolha inválida.")
    while nc not in {2,3,4,5,6}:
        print ("Escolha inválida.")
        while True:
            try:
                nc = int(input("Quer obter dados de quantos continentes? "))
                break
            except ValueError:
                print ("Escolha inválida.")
    lc = []
    lcp = []
    for o in range(nc):
        while True:
            try:
                continente = int(input("Qual o " + str(o+1) + "º continente? "))
                break
            except ValueError:
                print ("Escolha inválida.")
        while continente not in {1, 2, 3, 4, 5, 6}:
            print ("Escolha inválida.")
            while True:
                try:
                    continente = int(input("Qual o " + str(o+1) + "º continente? "))
                    break
                except ValueError:
                    print ("Escolha inválida.")
        if continente == 1:
            n = "Africa"
            c = "África"
        elif continente == 2:
            n = "North America"
            c = "América do Norte"
        elif continente == 3:
            n = "South America"
            c = "América do Sul"
        elif continente == 4:
            n = "Asia"
            c = "Ásia"
        elif continente == 5:
            n = "Europe"  
            c = "Europa"
        else:
            n = "Oceania"
            c = "Oceania"   
        lc.append(n)    
        lcp.append(c)
    
###Ordenar as populações###
    i = []
    for o in range(nc):
        i1 = 0
        while dados[i1]['location'] != lc[o] :
            i1 = i1+1
        i.append(i1)
    pop = []
    for o in range(nc):
         pop1 = float(dados[i[o]]['population'])
         pop.append(pop1)
    for k in range(nc-1):
        for u in range(k+1, nc):
            if pop[u]<pop[k] :
                pop[u], pop[k] = pop[k], pop[u]
                lc[u], lc[k] = lc[k], lc[u]
                lcp[u], lcp[k] = lcp[k], lcp[u]
    print (" ")  
    print("Por ordem crescente: ")
    print(" ")
    for l in range(nc):
       print(lcp[l] + " tem uma população igual a " + str(pop[l]))
       print (" ")      

###Menu datas###
modo = "Modo de Informação"    
print(f"{linha_de_iguais:^60s}")
print(f"{modo:^60s}")
print(f"{linha_de_iguais:^60s}") 
especifica = "1: Informação sobre uma data específica"
intervalo = "2: Informação sobre um intervalo de datas"
print(f"{especifica:^60s}")
print(f"{intervalo:^60s}")   
while True:
    try:
        resmodo = int(input("Pretende obter informação relativamente a uma data específica ou num intervalo de datas? "))
        break
    except ValueError:
        print ("Escolha inválida.")
while resmodo not in {1, 2}:
    print ("Escolha inválida.")
    while True:
        try:
            resmodo = int(input("Pretende obter informação relativamente a uma data específica ou num intervalo de datas? "))
            break
        except ValueError:
            print ("Escolha inválida.")

###Data específica###
if resmodo == 1: 
    data1 = str(data_especifica())
   
    h1 = 0
    h2 = 0
    h3 = 0
    
###Parâmetros data específica###
    parâmetros = "Lista de parâmetros:"
    a = "1: Número total de infetados"
    b = "2: Número total de mortes"
    c = "3: Número total de vacinados"
    d = "4: Novos casos diários"
    e = "5: Número de novas mortes"
    f = "6: Número de novos vacinados"
    print(f"{linha_de_iguais:^60s}")
    print(f"{parâmetros:^60s}")
    print(f"{linha_de_iguais:^60s}")
    print(f"{a:^60s}")
    print(f"{b:^60s}")
    print(f"{c:^60s}")
    print(f"{d:^60s}")
    print(f"{e:^60s}")
    print(f"{f:^60s}")
    while True:
        try:
            np = int(input("Qual o número de parâmetros que pretende saber? "))
            break
        except ValueError:
            print ("Escolha inválida.")
    while np not in {1, 2, 3, 4, 5, 6}:
        print ("Escolha inválida.")
        while True:
            try:
                np = int(input("Qual o número de parâmetros que pretende saber? "))
                break
            except ValueError:
                print ("Escolha inválida.")
    else:
        lp = []
    for i in range (1,np+1):
        while True:
            try:
                resp = int(input("Qual o "+ str(i) +"º parâmetro? "))
                break
            except ValueError:
                print ("Escolha inválida.")
        while resp not in {1,2,3,4,5,6}:
            print ("Escolha inválida.")
            while True:
                try:
                   resp = int(input("Qual o "+ str(i) +"º parâmetro? "))
                   break
                except ValueError:
                   print ("Escolha inválida.")
        lp.append(resp)
    print(" ")
    for j in range(np):
        a = lp[j]
        
### 1 continente data específica ###        
        if escolha == 1:
            while dados[h1]["location"] != n or dados[h1]["date"] != data1:
                h1 = h1+1
            if a == 1:
                if dados[h1]["total_cases"] == "":
                    print (c0 + " não tem registo de infetados na data escolhida.")
                    print(" ")
                else:    
                    print (c0 + ", no dia " + data1 + " registou " +\
                           dados[h1]["total_cases"] + " infetados.")
                    print (" ")    
            if a == 2:
                if dados[h1]["total_deaths"] == "":
                    print (c0 + " não tem registo de mortes na data escolhida.")
                    print(" ")
                else:    
                    print (c0 + ", no dia " + data1 + " registou " +\
                           dados[h1]["total_deaths"] + " mortes.")
                    print(" ")    
            if a == 3:
                if dados[h1]["people_fully_vaccinated"] == "":
                    print (c0 + " não tem registo de vacinados na data escolhida.")
                    print (" ")
                else:    
                    print (c0 + ", no dia " + data1 + " registou " +\
                           dados[h1]["people_fully_vaccinated"] + " vacinações.")
                    print (" ")    
            if a == 4:
                if dados[h1]["new_cases"] == "":
                    print (c0 + " não tem registo de novos casos na data escolhida.")
                    print (" ")
                else:    
                    print (c0 + ", no dia " + data1 + " registou " +\
                           dados[h1]["new_cases"] + " novos casos.")
                    print (" ")    
            if a == 5:
                if dados[h1]["new_deaths"] == "":
                    print (c0 + " não tem registo de novas mortes na data escolhida.")
                    print (" ")
                else:    
                    print (c0 + ", no dia " + data1 + " registou " +\
                           dados[h1]["new_deaths"] + " novas mortes.")
                    print (" ")    
            elif a == 6:
                if dados[h1]["people_fully_vaccinated"] == "":
                    print (c0 + " não tem registo de novos vacinados na data escolhida.")
                    print (" ")
                elif dados [h1-1]["people_fully_vaccinated"] == "":
                    dados [h1-1]["people_fully_vaccinated"] = 0  
                    novos_vacinados = str(float(dados[h1]["people_fully_vaccinated"])\
                                          - float(dados[h1-1]["people_fully_vaccinated"])) 
                    print (c0 + ", no dia " + data1 + " registou " +\
                           novos_vacinados + " novas vacinações.")
                    print (" ")
                else:    
                    novos_vacinados = str(float(dados[h1]["people_fully_vaccinated"])\
                                          - float(dados[h1-1]["people_fully_vaccinated"])) 
                    print (c0 + ", no dia " + data1 + " registou " +\
                           novos_vacinados + " novas vacinações.")
                    print (" ")
                    
### mais continentes data especifica ###                    
        else:  
            li = []
            for i in range(nc):
                while dados[h2]["location"] != lc[i] or dados[h2]["date"] != data1:
                    h2 = h2+1
                li.append(h2)
                h2 = 0
            for i in range(nc):    
                if a == 1:
                    if dados[li[i]]["total_cases"] == "":
                        print (lcp[i] + " não tem registo de infetados na data escolhida.")
                        print (" ")
                    else:    
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               dados[li[i]]["total_cases"] + " infetados.")
                        print (" ")
                if a == 2:
                    if dados[li[i]]["total_deaths"] == "":
                        print (lcp[i] + " não tem registo de mortes na data escolhida.")
                        print (" ")
                    else:    
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               dados[li[i]]["total_deaths"] + " mortes.")
                        print (" ")
                if a == 3:
                    if dados[li[i]]["people_fully_vaccinated"] == "":
                        print (lcp[i] + " não tem registo de vacinados na data escolhida.")
                        print (" ")
                    else:    
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               dados[li[i]]["people_fully_vaccinated"] + " vacinações.")
                        print (" ")
                if a == 4:
                    if dados[li[i]]["new_cases"] == "":
                        print (lcp[i] + " não tem registo de novos casos na data escolhida.")
                        print (" ")
                    else:    
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               dados[li[i]]["new_cases"] + " novos casos.")
                        print (" ")
                if a == 5:
                    if dados[li[i]]["new_deaths"] == "":
                        print (lcp[i] + " não tem registo de novas mortes na data escolhida.")
                        print (" ")
                    else:    
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               dados[li[i]]["new_deaths"] + " novas mortes.")
                        print (" ")    
                elif a == 6 :
                    if dados[li[i]]["people_fully_vaccinated"] == "":
                        print (lcp[i] + " não tem registo de novos vacinados na data escolhida.")
                        print (" ")
                    else:    
                        novos_vacinados1 = float(dados[li[i]]["people_fully_vaccinated"])\
                                              - float(dados[li[i]-1]["people_fully_vaccinated"])
                        print (lcp[i] + ", no dia " + data1 + " registou " +\
                               novos_vacinados1 + " novas vacinações.")
                        print (" ")
           
### comparar varios continentes data específica ###            
            while True:
                try:
                    comparar = int(input("Deseja comparar os dados dos 2 continentes? (1: Sim | 2: Não): "))
                    break
                except ValueError:
                    print ("Escolha inválida.")
            while comparar not in {1,2}:
                print ("Escolha inválida.")
                while True:
                    try:
                        comparar = int(input("Deseja comparar os dados dos 2 continentes? (1: Sim | 2: Não): "))
                        break
                    except ValueError:
                        print ("Escolha inválida.")
            else:
                if comparar == 1:    
                    if a == 1:
                        for i in range (nc):
                            if dados[li[i]]["total_cases_per_million"] == "":
                                    dados[li[i]]["total_cases_per_million"] = "0"
                        print ("Por ordem crescente:")
                        print(" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if float(dados[li[j]]["total_cases_per_million"]) < float(dados[li[i]]["total_cases_per_million"]):
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            print (lcp[i] + " tem " + dados[li[i]]["total_cases_per_million"] + " casos por milhão.")
                            print (" ")
                    elif a == 2:
                        for i in range (nc):
                            if dados[li[i]]["total_deaths_per_million"] == "":
                                    dados[li[i]]["total_deaths_per_million"] = "0"
                        print ("Por ordem crescente:")
                        print(" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if float(dados[li[j]]["total_deaths_per_million"]) < float(dados[li[i]]["total_deaths_per_million"]):
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            print (lcp[i] + " tem " + dados[li[i]]["total_deaths_per_million"] + " mortes por milhão.")
                            print (" ")
                    elif a == 3:
                        for i in range (nc):
                            if dados[li[i]]["people_fully_vaccinated_per_hundred"] == "":
                                    dados[li[i]]["people_fully_vaccinated_per_hundred"] = "0"
                        print ("Por ordem crescente:")
                        print (" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if float(dados[li[j]]["people_fully_vaccinated_per_hundred"]) < float(dados[li[i]]["people_fully_vaccinated_per_hundred"]):
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            print (lcp[i] + " tem " + dados[li[i]]["people_fully_vaccinated_per_hundred"] + " vacinados por cem.")
                            print (" ")
                    elif a == 4:
                        for i in range (nc):
                            if dados[li[i]]["new_cases_per_million"] == "":
                                    dados[li[i]]["new_cases_per_million"] = "0"
                        print ("Por ordem crescente:")
                        print (" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if float(dados[li[j]]["new_cases_per_million"]) < float(dados[li[i]]["new_cases_per_million"]):
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            print (lcp[i] + " tem " + dados[li[i]]["new_cases_per_million"] + " novos casos por milhão na data escolhida.")
                    elif a == 5:
                        for i in range (nc):
                            if dados[li[i]]["new_deaths_per_million"] == "":
                                    dados[li[i]]["new_deaths_per_million"] = "0"
                        print ("Por ordem crescente:")
                        print (" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if float(dados[li[j]]["new_deaths_per_million"]) < float(dados[li[i]]["new_deaths_per_million"]):
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            print (lcp[i] + " tem " + dados[li[i]]["new_deaths_per_million"] + " novas mortes por milhão na data escolhida.")
                    elif a == 6:
                        for i in range (nc):
                            if dados[li[i]]["people_fully_vaccinated_per_hundred"] == "":
                                    dados[li[i]]["people_fully_vaccinated_per_hundred"] = "0"
                        for i in range (nc):
                            if dados[li[i]-1]["people_fully_vaccinated_per_hundred"] == "":
                                    dados[li[i]-1]["people_fully_vaccinated_per_hundred"] = "0"            
                        novos_vacinados = []
                        for i in range(nc):
                            f = abs(float(dados[li[i]]["people_fully_vaccinated_per_hundred"]) - float(dados[li[i]-1]["people_fully_vaccinated_per_hundred"]))
                            novos_vacinados.append(f)
                        print ("Por ordem crescente:")
                        print (" ")
                        for i in range(nc):
                            for j in range (i+1, nc):
                                if novos_vacinados[j] < novos_vacinados[i]:
                                    novos_vacinados[i], novos_vacinados[j] = novos_vacinados[j], novos_vacinados[i]
                                    li[i], li[j] = li[j], li[i]
                                    lc[i], lc[j] = lc[j], lc[i]
                                    lcp[i], lcp[j] = lcp[j], lcp[i]
                        for i in range(nc):
                            a = novos_vacinados[i]
                            print (lcp[i] + f" tem {a:.2f} novas pessoas completamente vacinadas por 100.")
                        print (" ")

### intervalo de datas ###
else:            
    data2 = str(data_especifica()) 
    data3 = str(data_especifica())          
    
    j=0
    h1 = 0
    h2 = 0
    
### parâmetros ###    
    parâmetros = "Lista de parâmetros:"
    a = "1: Novos casos diários"
    b = "2: Número de novas mortes"
    c = "3: Número de novos vacinados"
    print(f"{linha_de_iguais:^60s}")
    print(f"{parâmetros:^60s}")
    print(f"{linha_de_iguais:^60s}")
    print(f"{a:^60s}")
    print(f"{b:^60s}")
    print(f"{c:^60s}")
    while True:
        try:
            np = int(input("Qual o número de parâmetros que pretende saber? "))
            break
        except ValueError:
            print ("Escolha inválida.")
    while np not in {1, 2, 3}:
        print ("Escolha inválida.")
        while True:
            try:
                np = int(input("Qual o número de parâmetros que pretende saber? "))
                break
            except ValueError:
                print ("Escolha inválida.")
    else:
        lp = []
    for i in range (1,np+1):
        while True:
            try:
                resp = int(input("Qual o "+ str(i) +"º parâmetro? "))
                break
            except ValueError:
                print ("Escolha inválida.")
        while resp not in {1,2,3}:
            print ("Escolha inválida.")
            while True:
                try:
                    resp = int(input("Qual o "+ str(i) +"º parâmetro? "))
                    break
                except ValueError:
                    print ("Escolha inválida.")
        lp.append(resp)
    print(" ")
    for j in range(np):
        a = lp[j]
        h1 = 0
        h2 = 0
        
### 1 continente intervalo de datas ###        
        if escolha == 1:
            while dados[h1]["location"] != n or dados[h1]["date"] != data2:
                h1 = h1+1      
            while dados[h2]["location"] !=n or dados[h2]["date"] != data3:
                h2 = h2+1
            if h2<h1:
                h2, h1 = h1, h2
            if a == 1:
                total = 0
                for i in range (h1, h2+1):
                    if dados[i]["new_cases"] != "":
                        total = total + float(dados[i]["new_cases"])
                print ("Neste intervalo de tempo, o número total de novos infetados foi " + str(total)) 
                print(" ")
            if a == 2:
                total = 0
                for i in range (h1, h2+1):
                    if dados[i]["new_deaths"] != "":                
                        total = total + float(dados[i]["new_deaths"])
                print ("Neste intervalo de tempo, o número total de novas mortes foi " + str(total))   
                print(" ")
            elif a == 3:
                novos_vacinados_int = []
                for i in range (h1, h2+1):
                    if dados[i]["people_fully_vaccinated"] == "":
                        dados[i]["people_fully_vaccinated"] = "0"
                    if dados[i-1]["people_fully_vaccinated"] == "":
                        dados[i-1]["people_fully_vaccinated"] = "0"        
                    f = float(dados[i]["people_fully_vaccinated"]) - float(dados[i-1]["people_fully_vaccinated"])
                    novos_vacinados_int.append(f)
                total = 0
                for i in range (len(novos_vacinados_int)):
                    if novos_vacinados_int[i] != 0:
                        total = total + float(novos_vacinados_int[i])
                print ("Neste intervalo de tempo o número total de novas pessoas completamente vacinadas foi " + str(total)) 
                print(" ")
        
### mais continentes intervalo de datas ###        
        else:    
            li = []
            h3 = 0
            h4 = 0
            for i in range(nc):
                while dados[h3]["location"] != lc[i] or dados[h3]["date"] != data2:
                    h3 = h3+1
                while dados[h4]["location"] != lc[i] or dados[h4]["date"] != data3:
                    h4 = h4+1    
                if h4<h3:
                    h4, h3 = h3, h4     
                li.append((h3,h4))
                h3 = 0
                h4 = 0
            inf = []  
            mort = []
            for i in range(nc):  
                if a == 1:
                    k = li[i][1]-li[i][0]
                    total = 0
                    for j in range(k+1):
                        total = total + float(dados[li[i][0]+j]["new_cases"])
                    inf.append(total)
                    print ("O número total de novos infetados no continente " + lcp[i] + " entre as datas " + dados[h3]["date"] + " e " + dados[h4]["date"] + " é " + str(total))
                if a == 2:
                    k = li[i][1]-li[i][0]
                    total = 0
                    for j in range(k+1):
                        total = total + float(dados[li[i][0]+j]["new_deaths"])
                    mort.append(total)
                    print ("O número total de novas mortes no continente " + lcp[i] + " entre as datas " + dados[h3]["date"] + " e " + dados[h4]["date"] + " é " + str(total))
                if a == 3:
                    novos_vacinados_int = []
                    k = li[i][1]-li[i][0]
                    total = 0
                    for j in range(k+1):
                        if dados[li[i][0]+k]["people_fully_vaccinated"] == "":
                            dados[li[i][0]+k]["people_fully_vaccinated"] = 0
                        if dados[li[i][0]+k-1]["people_fully_vaccinated"] == "":
                            dados[li[i][0]+k-1]["people_fully_vaccinated"] = 0
                        dif = float(dados[li[i][0]+k]["people_fully_vaccinated"]) - float(dados[li[i][0]+k-1]["people_fully_vaccinated"])
                        novos_vacinados_int.append(dif)
                        total = total + dif
                    print ("O número total de novos vacinados no continente " + lcp[i] + " entre as datas " + dados[h3]["date"] + " e " + dados[h4]["date"] + " é " + str(total))
        
        while True:
            try:
                todos = int(input("Deseja observar os dados diários nesse intervalo de tempo? (1: Sim | 2: Não): "))
                break
            except ValueError:
                print ("Escolha inválida.")
        while todos not in {1,2}:
            print ("Escolha inválida.")
            while True:
                try:
                    todos = int(input("Deseja observar os dados diários nesse intervalo de tempo? (1: Sim | 2: Não): "))
                    break
                except ValueError:
                    print ("Escolha inválida.")
        else:
            
### 1 continente mostrar todos os dados no intervalo ###            
            if todos == 1:
                if escolha ==1:
                    if a == 1:
                        for i in range (h1, h2+1):
                                if dados[i]["new_cases"] == "":
                                    print (c0 + " não tem registo de novos infetados na data " + str(dados[i]["date"]))
                                    print(" ")
                                else:
                                    print (c0 + " tem "+ str(dados[i]["new_cases"]) + " novos infetados no dia " + str(dados[i]["date"]))
                                    print(" ")
                    elif a == 2:
                        for i in range (h1, h2+1):
                                if dados[i]["new_deaths"] == "":
                                    print (c0 + " não tem registo de novas mortes na data " + str(dados[i]["date"]))
                                    print(" ")
                                else:
                                    print (c0 + " tem "+ str(dados[i]["new_deaths"]) + " novas mortes no dia " + str(dados[i]["date"]))
                                    print(" ")
                    elif a == 3:
                        for i in range (len(novos_vacinados_int)):
                                if novos_vacinados_int[i] == 0:
                                    print (c0 + " não tem registo de novos vacinados na data " + str(dados[i]["date"]))
                                    print('') 
                                else:
                                    print (c0 + " tem "+ str(novos_vacinados_int[i]) + " novas mortes no dia " + str(dados[i]["date"]))
                                    print(" ")
               
### mais continentes mostrar todos os dados no intervalo ###                
                else:
                    if a == 1:
                        for j in range(nc):
                            for i in range (li[j][0], li[j][1]+1):
                                    if dados[i]["new_cases"] == "":
                                        print (lcp[j] + " não tem registo de novos infetados na data " + str(dados[i]["date"]))
                                        print(" ")
                                    else:
                                        print (lcp[j] + " tem "+ str(dados[i]["new_cases"]) + " novos infetados no dia " + str(dados[i]["date"]))
                                        print(" ")
                    elif a == 2:
                        for j in range(nc):
                            for i in range (li[j][0], li[j][1]+1):
                                    if dados[i]["new_deaths"] == "":
                                        print (lcp[j] + " não tem registo de novas mortes na data " + str(dados[i]["date"]))
                                        print(" ")
                                    else:
                                        print (lcp[j] + " tem "+ str(dados[i]["new_deaths"]) + " novas mortes no dia " + str(dados[i]["date"]))
                                        print(" ")
                    ##### FALTA VER ISTO ####                    
                    elif a == 3:
                        for j in range(nc):
                            for i in range (len(novos_vacinados_int)):
                                    if novos_vacinados_int[i] == 0:
                                        print (lcp[j] + " não tem registo de novos vacinados na data " + str(dados[i]["date"]))
                                        print('') 
                                    else:
                                        print (lcp[j] + " tem "+ str(novos_vacinados_int[i]) + " novos vacinados no dia " + str(dados[i]["date"]))
                                        print(" ")
                                        
### comparar totais de mais continentes intervalo de datas ###                                        
        if escolha == 2:
            while True:
                try:
                    comparar = int(input("Deseja comparar os dados totais dos continentes nesse intervalo? (1: Sim | 2: Não): "))
                    break
                except ValueError:
                    print ("Escolha inválida.")
            while comparar not in {1,2}:
                print ("Escolha inválida.")
                while True:
                    try:
                        comparar = int(input("Deseja comparar os dados totais dos continentes nesse intervalo? (1: Sim | 2: Não): "))
                        break
                    except ValueError:
                        print ("Escolha inválida.")
            else:
                if comparar == 1:    
                        if a == 1:
                            print ("Por ordem crescente:")
                            print (" ")
                            for i in range(nc):
                                for j in range (i+1, nc):
                                    if float(inf[j]) < float(inf[i]):
                                        inf[i], inf[j] = inf[j], inf[i]
                                        lc[i], lc[j] = lc[j], lc[i]
                                        lcp[i], lcp[j] = lcp[j], lcp[i]
                            for i in range(nc):
                                print (lcp[i] + " tem " + str(inf[i]) + " novos infetados no intervalo de datas escolhidas.")
                        elif a == 2:
                            print ("Por ordem crescente:")
                            print (" ")
                            for i in range(nc):
                                for j in range (i+1, nc):
                                    if float(mort[j]) < float(mort[i]):
                                        mort[i], mort[j] = mort[j], mort[i]
                                        lc[i], lc[j] = lc[j], lc[i]
                                        lcp[i], lcp[j] = lcp[j], lcp[i]
                            for i in range(nc):
                                print (lcp[i] + " tem " + str(mort[i]) + " novas mortes no intervalo de datas escolhidas.")
                        ##### FALTA VER ISTO #####
                        elif a == 3:
                            for i in range (nc):
                                if dados[li[i]]["people_fully_vaccinated_per_hundred"] == "":
                                    dados[li[i]]["people_fully_vaccinated_per_hundred"] = "0"
                            for i in range (nc):
                                if dados[li[i]-1]["people_fully_vaccinated_per_hundred"] == "":
                                    dados[li[i]-1]["people_fully_vaccinated_per_hundred"] = "0"            
                            novos_vacinados = []
                            for i in range(nc):
                                f = abs(float(dados[li[i]]["people_fully_vaccinated_per_hundred"]) - float(dados[li[i]-1]["people_fully_vaccinated_per_hundred"]))
                                novos_vacinados.append(f)
                            print ("Por ordem crescente:")
                            print (" ")
                            for i in range(nc):
                                for j in range (i+1, nc):
                                    if novos_vacinados[j] < novos_vacinados[i]:
                                        novos_vacinados[i], novos_vacinados[j] = novos_vacinados[j], novos_vacinados[i]
                                        li[i], li[j] = li[j], li[i]
                                        lc[i], lc[j] = lc[j], lc[i]
                                        lcp[i], lcp[j] = lcp[j], lcp[i]
                            for i in range(nc):
                                a = novos_vacinados[i]
                                print (lcp[i] + f" tem {a:.2f} novas pessoas completamente vacinadas por 100.")
                            print (" ")
                       